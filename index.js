const axios = require("axios");
var XLSX = require("xlsx");
const fs = require("fs");
const express = require('express')
const app = express()
app.set('view engine', 'ejs');
app.use(express.static('public'))
const port = 3000
var data = [];
brands = {};
types = {};
const banned = require("./banned.json")
const ABC = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];


async function Update(){
  return new Promise(async (resolve, reject) => {
    var k = await axios.get("https://sztfh.hu/tevekenysegek/dohanyugyi-felugyelet/arak-kozzetetele/");
    k = k.data.split("Kapcsolódó letöltések")[1].split('href="')[1].split('"')[0];
    axios.get("https://sztfh.hu/"+k, {responseType: 'arraybuffer'}).then(response => {
      fs.writeFile('temp.xlsx', response.data, (err) => {
          if (err) throw err;
          var workbook = XLSX.utils.sheet_to_json(Object.values(XLSX.readFile("temp.xlsx").Sheets)[0], {header:true});
          workbook.shift();
          workbook = workbook.map(el => {
            values = Object.values(el);
            return {
              NDN: values[1],
              EAN: values[2],
              Type: values[3],
              Name: values[4].replace(/\s\s/g, " "),
              Brand: values[5].replace(/®/g, ""),
              Amount: `${values[6]}${values[7]=="g"?"":" "}${values[7]}`,
              Price: values[8],
              ValidFrom: values[9]
            }
          }).filter(el => !banned.NDN.includes(el.NDN) && !banned.EAN.includes(el.EAN))
          kk = {};
          kkk = {};

          workbook.forEach(el => {
            firstCharacter = el.Brand[0];
            if(!ABC.includes(firstCharacter))
            firstCharacter = "#";

            if(kk[firstCharacter] == undefined){
              kk[firstCharacter] = []
            }
            if(!kk[firstCharacter].includes(el.Brand))
            kk[firstCharacter].push(el.Brand)
          })
          workbook.forEach(el => {
            firstCharacter = el.Type[0];
            if(!ABC.includes(firstCharacter))
            firstCharacter = "#";

            if(kkk[firstCharacter] == undefined){
              kkk[firstCharacter] = []
            }
            if(!kkk[firstCharacter].includes(el.Type))
            kkk[firstCharacter].push(el.Type)
          })
          Object.keys(kk).forEach(el => {
            kk[el] = kk[el].sort((a,b) => (a > b) ? 1 : ((b > a) ? -1 : 0))
          })
          Object.keys(kkk).forEach(el => {
            kkk[el] = kkk[el].sort((a,b) => (a > b) ? 1 : ((b > a) ? -1 : 0))
          })

          resolve([workbook, kk, kkk])
          fs.unlink("temp.xlsx", (err2) => {
            if (err2) {
              console.error(err2)
              return
            }
          })
        });
    });
  })
}

async function main(){
  [data, brands, types] = await Update();

  app.listen(port, () => {
    console.log(`Dohany app listening on port ${port}`)
  })

  setInterval(async () => {  [data, brands, types] = await Update();}, 86400000)
}

main();

app.get('/', (req, res) => {
  res.render("index");
})

app.get('/brands/', (req, res) => {
  res.render("BrandSummary", {brands: brands, str:"brands"})
})

app.get('/brands/:BrandName', (req, res) => {
  res.render("Show", {products: data.filter(el => el.Brand == req.params.BrandName)})
})

app.get('/categories/', (req, res) => {
  res.render("BrandSummary", {brands: types, str:"categories"})
})

app.get('/categories/:CatName', (req, res) => {
  res.render("Show", {products: data.filter(el => el.Type == req.params.CatName)})
})
